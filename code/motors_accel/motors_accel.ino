
// Include the AccelStepper Library
#include <AccelStepper.h>

// Define pin connections
const int guillotinedirPin = 1;
const int guillotinestepPin = 2;
const int feederdirPin = 14;
const int feederstepPin = 15;
// Define motor interface type
#define motorInterfaceType 1


           // outside leads to ground and +5V
int val = 0;  // variable to store the value read
int analogPin = 26; // potentiometer wiper (middle terminal) connected to analog pin 3
                    // outside leads to ground and +5V

// Creates an instance
AccelStepper guillotine(motorInterfaceType, guillotinestepPin, guillotinedirPin);
AccelStepper feeder(motorInterfaceType, feederstepPin, feederdirPin);

void setup() {
	// set the maximum speed, acceleration factor,
	// guillotine initial speed and target position
	guillotine.setMaxSpeed(10000);
	guillotine.setAcceleration(5500);
	guillotine.setSpeed(9000);
	guillotine.moveTo(200*16*5);
  //feeder params
  feeder.setMaxSpeed(10000);
	feeder.setAcceleration(5500);
	feeder.setSpeed(9000);
	feeder.moveTo(200*16*0.5);
  // microstepping the guillotine and the feeder? can they share a bus? we will see 
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
  //microstepping the feeder

}

void loop() {
  val = analogRead(analogPin); 
  val = map(val, 0, 1023, 1, 10);
  guillotine.setSpeed(val*1000);
  Serial.println(val);          // debug value
	// Change direction once the motor reaches target position
  digitalWrite(3, HIGH);
  digitalWrite(4, HIGH);
  digitalWrite(5, HIGH);



  if (feeder.distanceToGo() == 0) 
		feeder.moveTo(-feeder.currentPosition());
    
	// Move the motor one step
	guillotine.run();
  feeder.run(); 


}



